"""

Thinking out loud
  - tuple vs dataclass entry
    - Benchmarks
  - categrorical support
    - User given list
    - Support different distributions? At least uniform
  - numerical support
   - float, int
  - datetime support
    - Actual datetimes, and strings of yy/mm/dd
  - string support?
    - what does this look like


Tests
  - Benchmarking
  - Generating
  - Spark at the end

"""
from __future__ import annotations

import random
from dataclasses import dataclass
from typing import Union, Any, Dict
from faker import Faker, generator

# Different generators. Possibly moving to a new file
@dataclass
class Uniform:
    minV: int
    maxVal: int


@dataclass
class Gauss:
    alpha: int
    beta: int



@dataclass
class Categorical:
    mapping: dict[Union[str, int], float]
    typ: str


@dataclass
class String:
    genType: str
    unique: bool


# Main configuration for our generator
@dataclass
class Config:
    size: int
    generators: dict[str, Union[Uniform, Gauss, String, Categorical]]

    @classmethod
    def from_dict(cls, json: Dict) -> Config:
        configs = [
        "gauss",
        "uniform",
        ]
        generators = {}
        size = 0
        maybeSize = json.get("size")
        if maybeSize:
            size = maybeSize
        if not json.get("generators"):
            raise Exception("No generators defined")

        for k,v in json.get("generators").items():
            if k == "gauss":
                generators[k] = Gauss(**v)

        return cls(size, generators)
    # def __init__(self, conf: Dict[str, Any]):
    #      self.size = conf.get("size"),
    #      self.generators = conf.get("generators")
    #     # _parseConfig(conf)

    # # def _parseConfig(self):


class Distributor:
    def __init__(self, seed: int, config: Dict):
        random.seed(seed)
        self.config = self._parseConfig(config)
        self.faker = Faker()
        self.target = self.config.size
        self.output = ()

        # for k, v in self.config.generators.items():

    def _parseConfig(self, conf: Dict[str, Any]) -> Config:
        t = Config(**conf)
        return t


    def genUniform(self):
        for _ in range(0, self.config.size):
            print(random.uniform(0, 1))

    def _uniform(self, minVal, maxVal):
        return random.uniform(minVal, maxVal)

    def genGauss(self):
        return

    def genCategortical(self):
        return