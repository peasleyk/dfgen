FROM apache/spark-py:v3.3.2

COPY ./ /app/
WORKDIR  /app

CMD ["/bin/bash"]