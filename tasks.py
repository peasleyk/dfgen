from taskr import runners, utils
from importlib.metadata import version

# Taskr config settings
DEFAULT = "all"
VENV_REQUIRED = True
IMAGE_NAME = "dfgen"


# Builds a wheel
def build() -> bool:
    return runners.run(["python -m build --wheel -n;", "echo 'Artifact:'; ls dist/"])


# Remove build artifacts, cache, etc.
def clean() -> bool:
    retValue = utils.cleanBuilds()

    if retValue:
        retValue = utils.cleanCompiles()

    return retValue


# Run tests
def test() -> bool:
    return runners.run("python -m pytest tests/ -vv")


# Run black
def fmt() -> bool:
    return runners.run("python -m black src/ tests/ tasks.py")


# Check types
def mypy() -> bool:
    return runners.run("python -m mypy src/ --exclude tests/")


# Check flake8
def lint() -> bool:
    return runners.run("python -m ruff check --fix src/ tests/ tasks.py")


# Runs all static analysis tools
def all() -> bool:
    return runners.run_conditional(lint, fmt, mypy)


# Runs a server based on a passed in variable
def dbuild() -> bool:
    return runners.run(f"podman build . -t {IMAGE_NAME} -f dockerfile")


def dconnect() -> bool:
    return runners.run(f"podman run --rm -it  {IMAGE_NAME}")


# Bump setup.py version
def bump(version: str = "") -> bool:
    return utils.bumpVersion(version)


# modify venv activation to set TASKR_DIR
def addTaskrEnv() -> bool:
    return utils.addTaskrToEnv()


# Some non python tasks


# Squash the branch
def squish() -> None:
    runners.run("git rebase -i `git merge-base HEAD master`")


# Tag the branch
def tag(ver: str = "") -> bool:
    if ver == "":
        ver = version("package-name")

    return runners.run([f"git tag v{ver};", "git push --tags"])
